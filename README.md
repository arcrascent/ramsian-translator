#The Ramsian Translator!#

**Feature:**

Converts normal English into Ramsian -- a rare language created by the great, freedom-loving, Richard M. Stallman.

**Uses:**

Nothing. But that is a side issue.

**Usage:**

Create a file called "rms.in", containing any english statements that you would like to translate into Ramsian. Then, run the Ramsian Translator and it will generate a file called "rms.out", containing the translation.

It is also to possible to use command line arguments with this program. Use --help for more details.