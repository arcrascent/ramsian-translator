/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unordered_map>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <string>
#include <vector>
#include <iterator>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>

/* General Rules:
 * a --> (if followed by 'l') oeh (else) ayy
 * a --> (if followed by 'b') oeeh
 * o --> (if followed by consonant) oeeh (else) oe
 * o --> (if followed by 'r') au
 * er --> (if at the end of a word) a'
 * u --> oeeh
 * i --> oy (if not first char of word)
 * her --> ayy
 * ee --> eeyyii
 * th --> d
 * I --> oy
 * You --> yoy
 * We --> wayy
 */

constexpr bool is_punct(char x) {
	return x == '?' || x == '.' || x == '!';
}

// Not the most efficient, but... it works well!
void foynd_replace(std::string& str, const std::string& find, const std::string& replace) {
	std::size_t pos = 0;
	while ((pos = str.find(find, pos)) != std::string::npos) {
		str.replace(pos, find.length(), replace);
		pos += find.length();
	}
}

std::string translate(std::string str) {
	std::unordered_map<std::string, std::string> lookup; // RMS loves O(1) lookup.
	/* DICTIONARY */
	lookup["fuck"] = "f'hoeck";
	lookup["ramsian"] = "rayymsiy'n";
	lookup["there"] = "dayyre";
	lookup["then"] = "den";
	lookup["they"] = "dayy";
	lookup["dick"] = "doyck";
	lookup["object"] = "ahpj'heyy'kt";
	lookup["objects"] = "ahpj'heyy'kt'hx";
	lookup["understand"] = "undershtayynd";
	lookup["think"] = "tink";
	lookup["thinks"] = "tinks";
	/* END DICTIONARY */
	for (int i = 0; i < str.size(); ++i)
		str[i] = std::tolower(str[i]);
		
	std::vector<std::string> words = [](std::string str)->std::vector<std::string> {
		std::istringstream buf(str);
		std::istream_iterator<std::string> beg(buf), end;
		std::vector<std::string> tokens(beg, end);
		return tokens;
	}(str);
	
	if (words.empty())
		return "";
	
	std::string retval;
	for (int i = 0; i < words.size(); ++i) {
		std::string output;
		if (lookup.find(words[i]) != lookup.end()) {
			retval += lookup[words[i]] + " ";
			continue;
		}
		
		if (i != 0 && (words[i] == "don't" || words[i] == "dont")
		 && (words[i - 1] == "i" || words[i - 1] == "you" || words[i - 1] == "we")) {
			retval += "doesn't ";
			continue;
		}
		
		if (i != 0 && (words[i] == "do" || words[i] == "does")
		 && (words[i - 1] == "i" || words[i - 1] == "you" || words[i - 1] == "we")) {
			retval += "does ";
			continue;
		}
		
		for (int j = 0; j < words[i].size(); ++j) {
			if (words[i][j] == 'a' && words[i] != "a"
			 && j != words[i].size() - 1) {
				if (j == words[i].size() - 1)
					output += "ayy";
				else if (words[i][j + 1] == 'l')
					output += "oeh";
				else if (words[i][j + 1] == 'b' || words[i][j + 1] == 'm'
				 || words[i][j + 1] == 'n')
					output += "oeeh";
				else
					output += "ayy";
			}

			else if (words[i][j] == 'o') {
				if (j == words[i].size() - 1) {
					output += "oe";
					break;
				}
				
				else if ([](char c)->bool {
					if (c != 'a' && c != 'e' && c != 'i'
					 && c != 'o' && c != 'u')
						return true;
					return false;
				}(words[i][j + 1]) && words[i][j + 1] != 'r')
					output += "oeeh";
				else if (words[i][j + 1] != 'r')
					output += "oe";
				else
					output += "au";
			}

			else if (words[i][j] == 'u') {
				if (j == words[i].size() - 1) {
					output += "oeeh";
				}
				
				if ([](char c)->bool {
					if (c != 'a' && c != 'e' && c != 'i'
					 && c != 'o' && c != 'u')
						return true;
					
					return false;
				}(words[i][j + 1]))
					output += "u";
				else
					output += "oeeh";
			}

			else if (words[i][j] == 'i' && j != 0) {
				if (j == words[i].size() - 1) {
					output += "oy";
					continue;
				}

				if (words[i][j + 1] != 'c' && words[i][j + 1] != 'n')
					output += "oy";
				else
					output += "i";
			}
			
			else if (words[i] == "i") {
				output = "oy";
				break;
			}
			
			else if (words[i] == "my") {
				output = "oy's";
				break;
			}
			
			else if (words[i] == "you") {
				output = "yoy";
				break;
			}
			
			else if (words[i] == "your") {
				output = "yoy's";
				break;
			}
			
			else if (words[i] == "we") {
				output = "wayy";
				break;
			}
			
			else if (words[i] == "our") {
				output = "woy's";
				break;
			}

			else if (j != words[i].size() - 1 && words[i][j] == 'e'
			 && words[i][j + 1] == 'l')
				output += "eyy";

			else if (j != words[i].size() - 1 && words[i][j] == 'e'
			 && words[i][j + 1] == 'r' && (j == 0 || words[i][j - 1] != 'h'))
				output += "a'";

			else if (j != words[i].size() - 1 && words[i][j] == 'e'
			 && j != 0 && words[i][j - 1] == 'h' && words[i][j] == 'e'
			 && words[i][j + 1] == 'r' && words[i] != "her")
				output += "ayy";
			
			else if (j <= words[i].size() - 2 && words[i][j] == 't'
			 && words[i][j + 1] == 'h' && words[i][j + 2] != 'i')
				output += "d";
			
			else if (j != words[i].size() - 1 && words[i][j] == 'e'
			 && words[i][j + 1] == 'e')
				output += "eeyyii";

			else
				output += words[i][j];
		}
		
		retval += output + " ";
	}
	
	// Alright, we converted the sounds, but there are some grammar features remaining :/
	// retval.pop_back();
	char restore = retval[retval.length() - 2]; // -2 to get passed the space.
	if (!is_punct(restore))
		restore = -1; // If there is no punctuation, then we should keep the space at the end for the grammar routine.

	// The grammar fix below searches for spaces.
	// For convenience (not above freedom of course), we add an extra space.
	else {
		retval.pop_back(); // Extra space.
		retval.pop_back(); // Punctuation.
		retval += ' '; // Add an extra space for use with the grammar routine. 
	}

	for (int i = 0; i < retval.length(); i++) {
		if (i + 1 < retval.length()) {
			const std::string check = retval.substr(i, 2);
			if (check == "oy" && (i == 0 || check[i - 1] != 'y')) {
				if (i + 2 >= retval.length())
					continue;

				// Oy [verb] --> Oy [verb]s.
				if (retval[i + 2] == ' ' && (i == 0 || retval[i - 1] == ' ')) {
					std::size_t pos = retval.find(' ', i + 3);
					if (pos == std::string::npos)
						continue;

					retval.replace(pos, 1, "s "); // Fix the oy grammar.
				}
			}
		}

		if (i + 2 < retval.length()) {
			const std::string check = retval.substr(i, 3);
			if (check == "yoy") {
				if (i + 3 >= retval.length())
					continue;

				// Similar to above:
				if (retval[i + 3] == ' ' && (i == 0 || retval[i - 1] == ' ')) {
					std::size_t pos = retval.find(' ', i + 4);
					if (pos == std::string::npos)
						continue;

					retval.replace(pos, 1, "s ");
				}
			}
		}

		// Check for we --> wayy
		if (i + 3 < retval.length()) {
			const std::string check = retval.substr(i, 4);
			if (check == "wayy") {
				if (i + 4 >= retval.length())
					continue;

				// Similar to above:
				if (retval[i + 4] == ' ' && (i == 0 || retval[i - 1] == ' ')) {
					std::size_t pos = retval.find(' ', i + 5);
					if (pos == std::string::npos)
						continue;

					retval.replace(pos, 1, "s ");
				}
			}
		}
	}
	
	// There may have been some mistakes, make sure we correct the verbs to be:
	foynd_replace(retval, " oeehms ", " is "); // LMAO!
	foynd_replace(retval, " iss ", " are ");
	foynd_replace(retval, " ayyres ", " is ");
	foynd_replace(retval, "oeehl", "oe'");
	foynd_replace(retval, "oehl", "oe'");
	retval.pop_back(); // Remove extra space.
	if (restore != -1)
		retval.push_back(restore); // Add user punctuation.
	
	return retval;
}

int main(int argc, const char** argv) {
	std::string str;
	if (argc == 1) {
		if (!std::fopen("rms.in", "r")) {
			std::cerr << "error: file \'rms.in\' missing." << "\n";
			return EXIT_FAILURE;
		}
		
		std::ifstream fin("rms.in");
		std::ofstream fout("rms.out");
		while (std::getline(fin, str))
			fout << translate(str) << "\n";
	}
	
	else if (argc == 2 && strcmp(argv[1], "--help") == 0) {
		std::printf("Ramsian Translator v1.0.0\n");
		std::printf("Copyright (c) 2015 Joonyoung Lee, Haoyuan Sun, and Wassim Omais\n");
		// std::printf("All Rights Reserved.\n");
		std::printf("This program is free software.\n\n");
		std::printf("Usage:\n");
		std::printf("Mode 1. translate word1 word2 word3 ...\n");
		std::printf("        (e.g. translate I don't know what you think)\n");
		std::printf("        Reads in an English statement from argv and writes the\n");
		std::printf("        Ramsian translation to standard output.\n");
		std::printf("Mode 2. translate\n");
		std::printf("        Reads in an English statement from file \'rms.in\' and\n");
		std::printf("        the Ramsian translation to file \'rms.out\'.\n");
		return 0;
	}
	
	else if (argc == 2 && strcmp(argv[1], "--version") == 0) {
		std::printf("Ramsian Translator v1.0.0\n");
		std::printf("Copyright (c) 2015 Joonyoung Lee, Haoyuan Sun, and Wassim Omais\n");
		// std::printf("All Rights Reserved.\n"); WTF JOONYOUNG THIS IS SO NONFREE!
		std::printf("This program is free software.\n");
	}
	
	else {
		for (int i = 1; i < argc; ++i) {
			for (int j = 0; j < std::strlen(argv[i]); ++j)
				str.push_back(argv[i][j]);
			if (i != argc - 1)
				str.push_back(' ');
		}
		
		std::cout << translate(str) << "\n";
	}	
	
	return 0;
}
